/*
 * scale: object for generating scales from presets
 * 
 * (c) 2022 Lauren Croney
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "m_pd.h"
#include "scaly.h"

static t_class *mode_class = NULL;

/* definitions for preset scales */
const t_float major[7] = {M2,M3,P4,P5,M6,M7,0};
const t_float minor[7] = {M2,m3,P4,P5,m6,m7,0};
const t_float dorian[7] = {M2,m3,P4,P5,M6,m7,0};
const t_float locrian[7] = {m2,m3,P4,TT,m6,m7,0};
const t_float lydian[7] = {M2,M3,TT,P5,M6,M7,0};
const t_float mixolydian[7] = {M2,M3,P4,P5,M6,m7,0};
const t_float phrygian[7] = {m2,m3,P4,P5,m6,m7,0};
const t_float japanese[5] = {M2,m3,P5,m6,0};
const t_float chromatic[12] = {m2,M2,m3,M3,P4,TT,P5,m6,M6,m7,M7,0};

/* data space for scale class */
typedef struct _mode {
    t_object x_obj; // object properties
    t_float *steps; // stored scale definition
    t_float octaves; // number of octaves to output
    t_atom scale[128]; // generated scale (allocate for all possible notes)
    int count; // number of notes per octave in scale
    t_symbol *type; // the name of the currently used preset
    t_outlet *l_out; // left outlet
} t_mode;

/* scale_float: handle incoming float messages by outputting a generated
 * scale starting at root.
 */
void mode_float(t_mode *x, t_floatarg root)
{
    const t_float *steps = NULL;

    /* check the preset name stored on creation */
    if (cmpsymbol(x->type, "major"))
        steps = major;
    else if (cmpsymbol(x->type, "minor"))
        steps = minor;
    else if (cmpsymbol(x->type, "dorian"))
        steps = dorian;
    else if (cmpsymbol(x->type, "locrian"))
        steps = locrian;
    else if (cmpsymbol(x->type, "lydian"))
        steps = lydian;
    else if (cmpsymbol(x->type, "mixolydian"))
        steps = mixolydian;
    else if (cmpsymbol(x->type, "phrygian"))
        steps = phrygian;
    else if (cmpsymbol(x->type, "japanese"))
        steps = japanese;
    else if (cmpsymbol(x->type, "chromatic"))
        steps = chromatic;
    else { // error out and refuse to output anything
        pd_error(x,"mode: invalid scale type");
        return; 
    }

    if (steps != NULL) {
#ifdef DEBUG
        post("outputting scale %s",x->type->s_name);
#endif
        int len = getscale(x->scale, root, x->octaves, steps); // generate scale
  
        outlet_list(x->l_out, &s_list, len, (t_atom *)x->scale);  // output scale
    }
}

/* mode_new: constructor for scale class. expects a single symbol arg. */
void *mode_new(t_symbol *s)
{
    t_mode *x = (t_mode *)pd_new(mode_class);

    x->type = s; // store the symbol arg as the preset name
    x->octaves = OCT_DEFAULT; // set default number of octaves to output

    /* set up inlets and outlets */
    x->l_out = outlet_new(&x->x_obj, &s_list);   // list outlet
    floatinlet_new(&x->x_obj, &x->octaves); // right inlet: set octave count

    return (void *)x;
}

/* mode_setup: setup function for mode class. */
void mode_setup(void)
{
    mode_class = class_new(gensym("mode"),
        (t_newmethod)mode_new,
        0, sizeof(t_mode),
        CLASS_DEFAULT,
        A_DEFSYMBOL, 0);
    
    class_addfloat(mode_class, mode_float); // float handler, left inlet

    post("scaly: loaded mode");
}