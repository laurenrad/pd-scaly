/*
 * newscale: generate a custom scale from a (list) scale definition
 *
 * (c) 2022 Lauren Croney
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "m_pd.h"
#include "scaly.h"
#include <stdlib.h>
#include <string.h>

static t_class *newscale_class = NULL;

/* newscale class data space */
typedef struct _newscale {
  t_object x_obj; // object properties
  t_float *steps; // stored scale definition
  t_float octaves; // number of octaves to output
  t_atom scale[128]; // generated scale (allocate for all possible notes)
  int count; // number of notes per octave in scale
  t_outlet *l_out; // left otulet
} t_newscale;

/* newscale_float: handle incoming float messages by outputting a generated
 * scale starting at root.
 */
void newscale_float(t_newscale *x, t_floatarg root)
{
    int len = getscale(x->scale, root, x->octaves, x->steps); // generate scale
  
    outlet_list(x->l_out, &s_list, len, (t_atom *)x->scale);  // output it
}

/* newscale_list: handle incoming list messages by replacing the stored
 * scale definition. */
void newscale_list(t_newscale *x, t_symbol *s, int argc, t_atom *argv)
{
    #ifdef DEBUG
        post("newscale_list() called");
    #endif
    
    /* free existing step list */
    freebytes(x->steps, (x->count*(sizeof(t_float))));
    
    /* allocate space for steps, including terminating zero */
    t_float *args = getbytes((argc+1)*sizeof(*args));

    x->count = argc+1; // store size for freeing later 

    /* if created without args, set up to just output the root note.
        additionally, do this if malloc fails */
    if (args == NULL)
        pd_error(x, "malloc failed in newscale_list");
    if (argc == 0 || args == NULL) { // created without args; just output the root note
        *args = 0;
        x->count = 1;
    } else {
        int i;
        for (i = 0; i < argc; i++) // copy and convert args
            args[i] = atom_getfloatarg(i, argc, argv);
        args[i] = 0; // add terminating zero
    }
    x->steps = args;

}

/* newscale_free: destructor for newscale class. also called by newscale_list
 * to free old scale definition. 
 */
void newscale_free(t_newscale *x)
{
    if (x->steps != NULL) {
        freebytes(x->steps, x->count*(sizeof(t_float)));
    }
    
    #ifdef DEBUG
        post("newscale free() called");
    #endif
}

/* newscale_new: constructor for newscale class. takes optional arg list for
   scale def. */
void *newscale_new(t_symbol *s, int argc, t_atom *argv)
{
    t_newscale *x = (t_newscale *)pd_new(newscale_class);

    /* allocate space for steps, including terminating zero */
    t_float *args = getbytes((argc+1)*sizeof(*args));
    x->count = argc+1; // store size for freeing later
    /* if created without args, set up to just output the root note.
        additionally, do this if malloc fails */
    if (args == NULL)
        pd_error(x, "malloc failed in newscale constructor");
    if (argc == 0 || args == NULL) { // created without args; just output the root note
        *args = 0;
        x->count = 1;
    } else {
        int i;
        for (i = 0; i < argc; i++) // copy and convert args
            args[i] = atom_getfloatarg(i, argc, argv);
        args[i] = 0; // add terminating zero
    }
    x->steps = args;

    x->octaves = OCT_DEFAULT; // set default number of octaves to output

    x->l_out = outlet_new(&x->x_obj, &s_list); // create list outlet
    floatinlet_new(&x->x_obj, &x->octaves); // create inlet 2: set octave

    return (void *)x;
}

/* newscale_setup: setup function for newscale class. */
void newscale_setup(void)
{
    newscale_class = class_new(gensym("newscale"),
        (t_newmethod)newscale_new,
        (t_method)newscale_free,
        sizeof(t_newscale),
        CLASS_DEFAULT, 
        A_GIMME, 0);

    class_addfloat(newscale_class, newscale_float); // float handler, left inlet
    class_addlist(newscale_class, newscale_list); // list handler, left inlet

    post("scaly: loaded newscale");

}