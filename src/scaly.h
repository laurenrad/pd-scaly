/*
 * common header file for the scaly library
 *
 * (c) 2022 Lauren Croney
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SCALY_H
#define SCALY_H

#include "m_pd.h"
#include <stdbool.h>

/* Global constants */
#define SCALY_LIB_VER "0.7.0" /*version string*/
#define COPYRIGHT_YEARS "2022" /* copyright years */
#define OCT_DEFAULT 1 /*default number of octaves*/
#define P1 0  /* Perfect unison */
#define m2 1  /* Minor second */
#define M2 2  /* Major second */
#define m3 3  /* Minor third */
#define M3 4  /* Major third */
#define P4 5  /* Perfect fourth */
#define TT 6  /* Tritone */
#define P5 7  /* Perfect fifth */
#define m6 8  /* Minor sixth */
#define M6 9  /* Major sixth */
#define m7 10 /* Minor seventh */
#define M7 11 /* Major seventh */
#define P8 12 /* Perfect octave */

/* Setup functions for object classes */
void newscale_setup(void);
void mode_setup(void);
void conform_setup(void);

/* Common helper functions -- see common.c */

int getscale(t_atom result[], t_float root, t_float octaves, 
      const t_float *steps); // generate scale

bool cmpsymbol(t_symbol *sym, char * str); // compare a symbol to a string

#endif /* SCALY_H */
