/*
 * common.c: common code and helper functions for scaly objects
 *
 * (c) 2022 Lauren Croney
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "scaly.h"
#include <stdbool.h>
#include <string.h>


/* cmpsymbol: compare a symbol pointer to a string */
bool cmpsymbol(t_symbol *sym, char * str)
{
    return strcmp(sym->s_name, str) == 0 ? true : false;
}

/*
  getscale: generate a scale from a scale definition.

  Calculates the scale given the root, number of octaves, an array
  describing the scale, and the size of an octave.
  Scale definition (steps) is to be zero-terminated.
  The generated scale is stored in result, which is assumed to be able to
  hold all possible MIDI notes (0-127).
  Returns size of result.
  
*/
int getscale(t_atom result[], t_float root, t_float octaves, 
      const t_float *steps)
{
  /* prevent root note from going out of bounds */
  root = (root <= 127) ? root : 127;
  root = (root > 0) ? root : 0;

  int len = 0; // length of result (and current index)
  int current = 0; // current calculation
  bool done = false; // flag if the valid note range is exceeded.
  t_float new_root = root; // for calculating the new root at each octave

  /* special case: negative octave values generate over the whole range,
     for convenient use with conform */
  if (octaves < 0) {
    root = (int) root % 12; // scale root down to the lowest octave
    new_root = root;
    octaves = 11; // span the whole range
  }

#ifdef DEBUG
  post("Root note: %.0f\tOctaves: %.0f", root, octaves);
#endif

  SETFLOAT(result+len,new_root); // store initial root note
  len++;

  for(int octave = 0; octave < octaves; octave++) {
    /* Stop when exceeding the maximum valid note */
    if (done)
      break;
  
    for(int degree = 0; *(steps+degree) != 0; degree++) {
      current = new_root + steps[degree]; // calc new note
      
      /* Flag if reaching the top of the valid note range */
      if(current > 127) {
	      len--;
	      done = true;
	      break;
      }  

      SETFLOAT(result+len,current);
      len++;     
    }

    new_root = root+(12*(octave+1)); // recalc root for new octave;
    if (new_root > 127) // recheck bounds
      done = true;  
    if (!done) {
      SETFLOAT(result+len,new_root); // store new root note
      len++;
    }


  }

  return len;

}