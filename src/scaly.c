/*
 * this file defines the top-level library class for scaly
 *
 * (c) 2022 Lauren Croney
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "m_pd.h"
#include "scaly.h"

static t_class *scaly_class = NULL;

typedef struct scaly {
  t_object scaly_obj;
} t_scaly;

void *scaly_new(void)
{
  t_scaly *x = (t_scaly *)pd_new(scaly_class);
  return (void *)x;
}

/* scaly_setup: setup for toplevel object. output a message and call all
   other setup functions. */
void scaly_setup(void)
{
  post("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-");
  post("Loading Scaly Library "SCALY_LIB_VER);
  post("(c) "COPYRIGHT_YEARS" Lauren Croney");
  post("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-");

  scaly_class = class_new(gensym("scaly"),
			     (t_newmethod)scaly_new,
			     0, sizeof(t_scaly),
			     CLASS_DEFAULT,
			     0, 0);

  /* call object setup functions*/
  newscale_setup();
  mode_setup();
  conform_setup();
  
  
}
