/*
 * conform: object which, given a note, outputs one which fits a given scale
 * This is a "naive" version which really just returns the nearest float in
 * the stored list. This assumes the list is sorted for correct results, but
 * should be safe if it's not.
 * 
 * (c) 2022 Lauren Croney
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <https://www.gnu.org/licenses/>.
 */


#include "m_pd.h"

static t_class *conform_class = NULL;

/* conform class data space */
typedef struct _conform {
    t_object x_obj; // object properties
    t_float *scale; // stored scale to conform note to
    int size; // size of scale
    t_outlet *l_out; // left outlet
} t_conform;

void conform_free(t_conform *x);

/* conform_float: handle incoming float messages by outputting a conformed
 * note to the left inlet
 */
void conform_float(t_conform *x, t_floatarg note)
{
    /* starting from the back, output the first note lower than note */
    for (int i = x->size-1; i > 0; i--) {
        if (x->scale[i] <= note) {
            outlet_float(x->l_out, x->scale[i]);
            return;
        }
    }

    /* there are two possibilities left if we are here */
    if (*(x->scale) == -1) // 1. the list was empty, and has been flagged
        outlet_float(x->l_out, note); // just output note unchanged
    else // 2. this means the note was lower than any in scale
        outlet_float(x->l_out, *(x->scale)); // so output bottom of scale

}   

/* conform_list: handle incoming list messages by replacing the stored
 * scale with a new one
 */
void conform_list(t_conform *x, t_symbol *s, int argc, t_atom *argv)
{
    conform_free(x); // free old list

    t_float *args = getbytes(argc*sizeof(*args)); // allocate new list

    /* if created without args, set to -1 to flag as empty so the note can
        pass through unchanged. additionally, do this if malloc fails */
    if (args == NULL)
        pd_error(x, "malloc failed in conform constructor");
    if (argc == 0 || args == NULL) {
        *args = -1;
        x->size = 1;
    } else {
        for (int i = 0; i < argc; i++)
            args[i] = atom_getfloatarg(i, argc, argv); // copy args and convert
        x->size = argc;
    }

    x->scale = args; // point to the result

}

/* conform_free: destructor for conform class. also called when replacing the
 * current stored scale on an incoming list message.
 */
void conform_free(t_conform *x)
{
    if (x->scale != NULL) {
        freebytes(x->scale, (x->size*sizeof(t_float)));
    }

    #ifdef DEBUG
        post("conform_free() called");
    #endif
}

/* conform_new: constructor for conform class. */
void *conform_new(t_symbol *s, int argc, t_atom *argv)
{
    t_conform *x = (t_conform *)pd_new(conform_class);

    t_float *args = getbytes(argc*sizeof(*args));

    /* if created without args, set to -1 to flag as empty so the note can
        pass through unchanged. additionally, do this if malloc fails */
    if (args == NULL)
        pd_error(x, "malloc failed in conform constructor");
    if (argc == 0 || args == NULL) {
        *args = -1;
        x->size = 1;
    } else {
        for (int i = 0; i < argc; i++)
            args[i] = atom_getfloatarg(i, argc, argv); // copy args and convert
        x->size = argc;
    }

    x->scale = args;

    x->l_out = outlet_new(&x->x_obj, &s_float); /* outlet: conformed note */

    return (void *)x;
}

/* conform_setup: setup function for conform class. */
void conform_setup(void)
{
    conform_class = class_new(gensym("conform"),
    (t_newmethod)conform_new,
    (t_method)conform_free,
    sizeof(t_conform),
    CLASS_DEFAULT,
    A_GIMME, 0);

    class_addfloat(conform_class, conform_float); // float handler, left inlet
    class_addlist(conform_class, conform_list); // list handler, left inlet

    post("scaly: loaded conform");
}