# Makefile for Scaly using pd-lib-builder

PDLIBBUILDER_DIR=pd-lib-builder
DESTDIR=build
lib.name=scaly
make-lib-executable=yes
class.sources = \
		src/newscale.c \
		src/mode.c \
		src/conform.c
lib.setup.sources=src/scaly.c
shared.sources=src/common.c
datafiles = doc/scaly-help.pd \
			doc/mode-help.pd \
			doc/conform-help.pd \
			doc/newscale-help.pd \
			doc/README.txt \
			COPYING \
			abstractions/prettynote.pd \
			abstractions/arpeggi.pd
# suppress -Wunused-parameter because list handlers and GIMME constructors require useless selector args
# suppress -Wcast-function-type because of required cast for constructors in class_new
cflags=-Wno-unused-parameter -Wno-cast-function-type #some warnings temporarily disabled
# uncomment below for debug build
#cflags=-D DEBUG -Wno-unused-parameter -Wno-cast-function-type #some warnings temporarily disabled
include $(PDLIBBUILDER_DIR)/Makefile.pdlibbuilder

