scaly: a simple scale library for puredata
(c) 2022 Lauren Croney
See COPYING for license information.

-------------------------------------------------------------------------------
I. Installation
-------------------------------------------------------------------------------
 - To install from source, simply make && make install. Copy the scaly
   folder from the build directory into your Pd externals directory.
 - To install from binary, simply go to bin, then the folder for your
   OS and arch, and copy the scaly folder into your Pd externals directory.

-------------------------------------------------------------------------------
II. Objects
-------------------------------------------------------------------------------

a.) mode
This object outputs preset scales given a root note. The name is a bit of a 
misnomer but "scale" was already taken by an unrelated object in a ubiquitous
library, so this was chosen for convenience.
	ARGS:
		1. Preset name (symbol). This selects which scale preset to use,
		from the following:
			major
			minor
			dorian
			phrygian
			lydian
			mixolydian
			locrian
			japanese
			chromatic
		If this arg is not set, scale will report an error to the console
		and produce no output.
	INPUTS:
		1. (left inlet, float) Root note: when received, output the scale
		starting at the given root note. (Expects a valid MIDI note number.)
		2. (right inlet, float) Octaves: set the number of octaves to
		generate the scale for. If octaves is less than 0, the scale
		will be generated over as wide a range as possible, transposing the
		root note down to the lowest octave. This is useful when combined with
		the conform object.
	OUTPUTS:
		1. (list) The generated scale, as a list of floats.
	QUIRKS/NOTES:
		1. If the number of octaves is 0, only the input root note will be
		output.
		2. Generally, the output scale will end on the root note. However, if
		the requested range exceeds the maximum MIDI note (127), it will end
		on the highest possible note. This includes when octave < 0.
	

b.) newscale
This object outputs a custom scale given a scale definition.
	ARGS:
		1. Scale definition (list). This is a list of the scale
		degrees making up the scale, excluding the tonic. Each
		number is an interval relative to the tonic, in steps.
		For example, the major scale would be:
		2 4 5 7 9 11
		There is no strict enforcement of expected norms, like
		the scale being in ascending order or any order.
	INPUTS:
		1. (left inlet, float) Root note: when received, output the scale
		starting at the given root note. (Expects a valid MIDI note number.)
		2. (left inlet, list) Scale definition: when received, replaces the
		currently stored scale definition with a new one. See ARGS.
		3. (right inlet, float) Octaves: set the number of octaves to
		generate the scale for. If octaves is less than 0, the scale will be
		generated over as wide a range as possible, transposing the root note
		down to the lowest octave. This is useful when combined with the
		conform object.
	OUTPUTS:
		1. (list) The generated scale, as a list of floats.
	QUIRKS/NOTES:
		1. If the number of octaves is 0, only the input root note will be
		output.
		2. Generally, the output scale will end on the root note. However, if
		the requested range exceeds the maximum MIDI note (127), it will end
		on the highest possible note. This includes when octave < 0.
		3. If scale definition is uninitialized, the root note will be output
		over different octaves (the scale definition will effectively just be
		'0'.)
		
c.) conform
This object converts incoming notes to the nearest note in a given scale.
Most useful when combined with output from scale or newscale.
This is a fairly naive algorithm, so if the note isn't in the specified scale,
it will just be pulled down to the next note that is. Otherwise, it passes
through unchanged. This object will hopefully be smarter and more useful in
the next major release, due out October 2039.
	ARGS:
		1. (optional) Notes (list). List of notes to conform input to.
	INPUTS:
		1. (left inlet, float) Note: A MIDI note to be conformed.
		2. (left inlet, list) Notes: A new scale to conform to.
	OUTPUTS:
		1. (float) The conformed MIDI note.
	QUIRKS/NOTES:
		1. If no args are specified and the scale is not initialized,
		the input will pass through unchanged.
	
-------------------------------------------------------------------------------
III. Additional Notes
-------------------------------------------------------------------------------
a.) Included abstractions
	Included in the package is a couple potentially useful abstractions:
		1. prettynote - output a MIDI note in a readable format.
		2. arpeggi - output elements in a list one at a time with a delay.
b.) Other
	Thanks as always to Miller Puckette and IOhannes m zmölnig. Contact me with
	any questions, concerns, or suggestions at lauren@cybertapes.com .
